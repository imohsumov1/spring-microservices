package com.ingress.ms6demo;

import com.ingress.ms6demo.services.ApplicationYamlDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class Ms6demoApplication implements CommandLineRunner {

	private final ApplicationYamlDetails applicationYamlDetails;

	public static void main(String[] args) {
		SpringApplication.run(Ms6demoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.trace("Application.yaml {} {} {}",applicationYamlDetails.getVersion(), applicationYamlDetails.getDevelopers(), applicationYamlDetails.getScores());
	}
}
