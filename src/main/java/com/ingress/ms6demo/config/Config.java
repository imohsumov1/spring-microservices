package com.ingress.ms6demo.config;

import com.ingress.ms6demo.services.StudentService;
import com.ingress.ms6demo.services.StudentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Slf4j
@Configuration
public class Config {

//    @Bean
//    public StudentService studentService(){
//        log.trace("Created new student service");
//        return new StudentServiceImpl();
//    }
    @Bean
    public ModelMapper getMapper(){
        return new ModelMapper();
    }
}
