package com.ingress.ms6demo.controller;

import com.ingress.ms6demo.dto.CreateStudentDTO;
import com.ingress.ms6demo.dto.StudentDTO;
import com.ingress.ms6demo.services.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/v1/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    public StudentDTO get(@PathVariable Long id) {
        log.trace("Get request id {}", id);
        return studentService.getStudent(id);
    }
    @PostMapping
    public ResponseEntity<StudentDTO> create(@RequestBody CreateStudentDTO studentDTO){
        log.trace("Post request body {}", studentDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.createStudent(studentDTO));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        log.trace("Delete request id {}", id);
        studentService.deleteStudent(id);
    }

    @PutMapping("/{id}")
    public StudentDTO update(@PathVariable Long id, @RequestBody CreateStudentDTO studentDTO){
        log.trace("Update car by id {}",id);
        return studentService.updateStudent(id, studentDTO);
    }

}
