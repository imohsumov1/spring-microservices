package com.ingress.ms6demo.dto;

import lombok.Data;

@Data
public class CreateStudentDTO {
    private String firstName;
    private String lastName;
}
