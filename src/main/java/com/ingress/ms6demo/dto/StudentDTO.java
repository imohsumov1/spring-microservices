package com.ingress.ms6demo.dto;

import lombok.Data;

@Data
public class StudentDTO {
    private Long id;

    private String firstName;

    private String lastName;
}
