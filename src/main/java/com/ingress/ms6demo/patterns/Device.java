package com.ingress.ms6demo.patterns;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Device {
    private String brand;
    private String model;
    private String year;

}
