package com.ingress.ms6demo.repository;

import com.ingress.ms6demo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long>{

}
