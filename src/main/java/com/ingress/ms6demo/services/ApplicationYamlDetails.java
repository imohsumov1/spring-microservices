package com.ingress.ms6demo.services;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationYamlDetails {

    private String version;
    private List developers;
    private Map<String, Integer> scores;
}
