package com.ingress.ms6demo.services;

import com.ingress.ms6demo.dto.CreateStudentDTO;
import com.ingress.ms6demo.dto.StudentDTO;

public interface StudentService {
    public StudentDTO getStudent(Long id);
    public StudentDTO createStudent(CreateStudentDTO dto);
    public StudentDTO updateStudent(Long id, CreateStudentDTO dto);
    public void deleteStudent(Long id);


}
