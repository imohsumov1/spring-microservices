package com.ingress.ms6demo.services;

import com.ingress.ms6demo.dto.CreateStudentDTO;
import com.ingress.ms6demo.dto.StudentDTO;
import com.ingress.ms6demo.entity.Student;
import com.ingress.ms6demo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    public StudentDTO getStudent(Long id){
        Optional<Student> optionalStudent = studentRepository.findById(id);
        return mapper.map(optionalStudent.get(), StudentDTO.class);
    }
    @Override
    public StudentDTO createStudent(CreateStudentDTO dto) {
        Student save = studentRepository.save(mapper.map(dto, Student.class));
        return mapper.map(save, StudentDTO.class);
    }
    @Override
    public StudentDTO updateStudent(Long id, CreateStudentDTO dto) {
        Student student = mapper.map(dto, Student.class);
        student.setId(id);
        studentRepository.save(student);
        return mapper.map(student, StudentDTO.class);
    }
    @Override
    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }

}
